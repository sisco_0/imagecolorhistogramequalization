#include <cuda.h>
#include <nvToolsExt.h>
#include <cstdio>
#include <opencv2/opencv.hpp>
#include <vector>
#include <ctime>
#include <fstream>

__constant__ uchar d_relationship[3][256];

using namespace cv;
using namespace std;
__global__ void histogramEqualization(
		uchar *inputData,
		uchar *outputData,
		size_t width,
		size_t height,
		int c,
		int pixelsPerThread)
{
	size_t idx = blockIdx.x*blockDim.x+threadIdx.x;
	size_t pixelStart = pixelsPerThread*idx;
	if(pixelStart>=width*height) return;
	size_t pixelEnd =
			min(width*height,pixelsPerThread*(idx+1));
	for(int i=pixelStart;i<pixelEnd;i++)
		outputData[i]=d_relationship[c][inputData[i]];
}
__global__ void fillHistogramGrayscaleImage(uchar *imageGrayscale, int *g_histogram, size_t width, size_t height, int pixelsPerThread)
{
	//blockDim.x must be equal to 256
	/*
	 * Global memory access: pixelsPerThread+1
	 * Calculations: 5 products, pixelsPerThread+1 add
	 */
	__shared__ int b_histogram[256];
	b_histogram[threadIdx.x] = 0;
	__syncthreads();
	size_t idx = blockIdx.x*blockDim.x+threadIdx.x;
	size_t pixelStart = pixelsPerThread*idx;
	if(pixelStart>=width*height) return;
	size_t pixelEnd =
			min(width*height,pixelsPerThread*(idx+1));
	for(int i=pixelStart;i<pixelEnd;i++)
		atomicAdd(&b_histogram[imageGrayscale[i]],1);
	__syncthreads();
	atomicAdd(&g_histogram[threadIdx.x],b_histogram[threadIdx.x]);
}

int main(int argc, char **argv)
{
	if(argc!=2)
	{
		printf("[INFO] Usage: %s image.jpg\n",argv[0]);
		exit(0);
	}
	Mat imageColor;
	imageColor = imread(argv[1], CV_LOAD_IMAGE_COLOR);
	if(!imageColor.data)
	{
		printf("[ERR] Couldn't open %s\n",argv[1]);
		exit(0);
	}
	Mat bgr[3];
	split(imageColor,bgr);

	/*namedWindow("Unequalized image", WINDOW_NORMAL | WND_PROP_FULLSCREEN | WND_PROP_ASPECT_RATIO);
	imshow("Unequalized image",imageGrayscale);*/
	int width=imageColor.cols,height=imageColor.rows;
	size_t channelBytesCount = sizeof(uchar)*width*height;
	int h_histogram[3][256],*d_histogram;
	cudaMalloc((void **)&d_histogram,sizeof(int)*256*3);
	cudaMemset(d_histogram,0,sizeof(int)*256*3);
	uchar *d_imageChannelData;
	cudaMalloc((void **)&d_imageChannelData,channelBytesCount*3);
	int pixelsPerThread = 32;
		dim3 blockSize(256);
		dim3 gridSize(ceil((float)height*(float)width/(float)blockSize.x/(float)pixelsPerThread));

	for(int c=0;c<3;c++)
	{
		cudaMemcpy(d_imageChannelData+c*channelBytesCount,
				bgr[c].data,
				channelBytesCount,
				cudaMemcpyHostToDevice);
		fillHistogramGrayscaleImage<<<gridSize,blockSize>>>(
					d_imageChannelData+c*channelBytesCount,
					&d_histogram[c*256],
					width,
					height,
					pixelsPerThread
		);
	}
	cudaDeviceSynchronize();

	cudaMemcpy(h_histogram,d_histogram,
			sizeof(int)*256*3,
			cudaMemcpyDeviceToHost);
	ofstream ficheroSalidaHandle;

	float h_relFrequenciesHistogram[3][256];
	for(int c=0;c<3;c++)
		for(int i=0;i<256;i++)
			h_relFrequenciesHistogram[c][i]=
					(float)h_histogram[c][i]/((float)height*(float)width);
	uchar h_relationship[3][256];
	for(int c=0;c<3;c++)
	{
		float relFrequencyAccum = 0.0;
		for(int i=0;i<256;i++)
		{
			relFrequencyAccum+=
					h_relFrequenciesHistogram[c][i];
			h_relationship[c][i]=floor(255.0*relFrequencyAccum);
		}
	}
	uchar *d_imageChannelDataEqualized;
	cudaMalloc((void **)&d_imageChannelDataEqualized,channelBytesCount*3);
	cudaMemcpyToSymbol(d_relationship,h_relationship,
			sizeof(uchar)*256*3,0,
			cudaMemcpyHostToDevice);
	for(int c=0;c<3;c++)
		histogramEqualization<<<gridSize,blockSize>>>(
			d_imageChannelData+c*channelBytesCount,
			d_imageChannelDataEqualized+c*channelBytesCount,
			width,
			height,
			c,
			pixelsPerThread
		);
	cudaDeviceSynchronize();
	uchar *h_imageChannelDataEqualized;
	h_imageChannelDataEqualized =
			(uchar *)malloc(channelBytesCount*3);
	cudaMemcpy(h_imageChannelDataEqualized,
			d_imageChannelDataEqualized,
			channelBytesCount*3,
			cudaMemcpyDeviceToHost);
	Mat b(
			height,
			width,
			CV_8U,
			(void *)h_imageChannelDataEqualized);
	Mat g(
			height,
			width,
			CV_8U,
			(void *)h_imageChannelDataEqualized+channelBytesCount);
	Mat r(
			height,
			width,
			CV_8U,
			(void *)h_imageChannelDataEqualized+channelBytesCount*2);
	vector<Mat> channels = {b,g,r};
	Mat equalizedImage;
	merge(channels,equalizedImage);
/*	namedWindow("Equalized image",  WINDOW_NORMAL | WND_PROP_FULLSCREEN | WND_PROP_ASPECT_RATIO);
	imshow("Equalized image",equalizedImage);

	waitKey(0);*/
	imwrite("Salida.jpg",equalizedImage);

	cudaFree(d_histogram);
	cudaFree(d_imageChannelData);
	equalizedImage.release();
	imageColor.release();
	return 0;
}
